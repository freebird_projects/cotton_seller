import React,{useEffect} from 'react'
import { Provider } from 'react-native-paper'
import { NavigationContainer } from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack';
import { theme } from './src/core/theme'
import { View, Text } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  LoginScreen,
  RegisterScreen,
  ForgotPasswordScreen,
  SplashScreen,
  Dashboard,
  SetPasswordScreen,
  VerifyOtpScreen,
  ChangePasswordScreen,
  SearchSelectSeller,
  DealDetails,
  NegotiateDetails,
  NotificationSelectSeller,
  MyPostDetails,
  MyContractFilter,
  MyContractDetails,
  MultipleNegotiationList
} from './src/screens'
import EditProfile from './src/components/EditProfile'
import Imageshow from './src/components/Imageshow'

import NewsSingle from './src/components/NewsSingle'
import Custom from './src/components/Custom'
import Plan from './src/components/Plan'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from './src/components/responsive-ratio';
import Wallet from './src/components/Wallet'
import { FirstRoute, SecondRoute, ThirdRoute } from './src/components/CalculatorView'

import firebase from '@react-native-firebase/app';
import messaging from '@react-native-firebase/messaging';
import EncryptedStorage from 'react-native-encrypted-storage';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

const Stack = createStackNavigator()
const Tab = createMaterialTopTabNavigator();

const tabnavi = ({ navigation }) => {
  console.log('nabvi', navigation)
  return (
    <View style={{ flex: 1, backgroundColor: '#333', }}>
      <View style={{
        flexDirection: 'row', paddingHorizontal: wp(5),
        marginTop: hp(2), height: hp(5), alignItems: 'center', justifyContent: 'space-between'
      }}>
        <Ionicons name='chevron-back-outline' size={hp(3)} color='#fff' style={{ width: wp(30) }} onPress={() => navigation.goBack()} />
        <Text style={{ alignSelf: 'center', color: '#fff', fontSize: hp(3), fontFamily: 'Poppins - Regular' }}>Calculator</Text>
        <View style={{ width: wp(30) }} />

      </View>
      <View style={{
        flex: 1,
        width: '100%',
        height: hp(86),
        paddingBottom: 30,
        paddingTop: hp(3),
        marginTop: hp(2),
        backgroundColor: 'white',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
      }}>
        <Tab.Navigator tabBarOptions={{
          labelStyle: { fontSize: hp(2), fontFamily: 'Poppins - Regular' },
          activeTintColor: theme.colors.primary,
          inactiveTintColor: '#afafaf',
          indicatorStyle: { backgroundColor: theme.colors.primary }
        }}>
          <Tab.Screen name="Ginning" component={FirstRoute} />
          <Tab.Screen name="Spinning" component={SecondRoute} />
          <Tab.Screen name="Exports" component={ThirdRoute} />
        </Tab.Navigator>
      </View>
    </View>
  );
}

const App = () => {

  const credentials = {
    clientId: '344769026558-brnhdibbgm0t5slebcc769go7hp07qbm.apps.googleusercontent.com',
    appId: '1:344769026558:android:31b9350842bf06912e08af',
    apiKey: 'AIzaSyDQFD_xWk9itib5H-sGiJ-Utc1IL-8QiRo',
    storageBucket: 'cotton-seller.appspot.com',
    databaseURL: 'https://databasename.firebaseio.com',
    messagingSenderId: '344769026558',
    projectId: 'cotton-seller',
  };

  async function onAppBootstrap() {
    // Register the device with FCM

    if (!firebase.apps.length) {
      await firebase.initializeApp(credentials);
    }

    await messaging().registerDeviceForRemoteMessages();

    // Get the token
    const token = await messaging().getToken();

    console.log('token', token)

    await EncryptedStorage.setItem('FCMToken', token);

    // Save the token
  }


  async function onMessageReceived(message) {
    console.log('message', message)
  }

  useEffect(() => {
    onAppBootstrap()
    
    // const unsubscribe = messaging().onMessage(async remoteMessage => {
    //   Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
    // });

    messaging().onMessage(onMessageReceived);
    messaging().setBackgroundMessageHandler(onMessageReceived);

    // return unsubscribe;
  }, []);

  return (
    <Provider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="SplashScreen"
          screenOptions={{
            headerShown: false,
          }}
        >
          
          <Stack.Screen name="LoginScreen" component={LoginScreen} />
          <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
          <Stack.Screen
            name="ForgotPasswordScreen"
            component={ForgotPasswordScreen}/>
            <Stack.Screen
            name="SetPasswordScreen"
            component={SetPasswordScreen}/>
            <Stack.Screen
            name="ChangePasswordScreen"
            component={ChangePasswordScreen}/>
          <Stack.Screen name="SplashScreen" component={SplashScreen} />
          <Stack.Screen name="Dashboard" component={Dashboard} />
          <Stack.Screen name="VerifyOtpScreen" component={VerifyOtpScreen} />
		  <Stack.Screen name="MyContractDetails" component={MyContractDetails} />
		  <Stack.Screen name="MyContractFilter" component={MyContractFilter} />
          <Stack.Screen name="SearchSelectSeller" component={SearchSelectSeller} />
          <Stack.Screen name="DealDetails" component={DealDetails} />
          <Stack.Screen name="NegotiateDetails" component={NegotiateDetails} />
          <Stack.Screen name="NotificationSelectSeller" component={NotificationSelectSeller} />
          <Stack.Screen name="MyPostDetails" component={MyPostDetails} />
	  <Stack.Screen name="MultipleNegotiationList" component={MultipleNegotiationList} />
          <Stack.Screen name="EditProfile" component={EditProfile} />
          <Stack.Screen name="NewsSingle" component={NewsSingle} />
          <Stack.Screen name="Plan" component={Plan} />
          <Stack.Screen name="Wallet" component={Wallet} />
          <Stack.Screen name="Custom" component={Custom} />
          <Stack.Screen name="Calculator" component={tabnavi} />
          <Stack.Screen name="Imageshow" component={Imageshow} />

          

        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}

export default App
