import React from 'react';
import { View, Image, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        padding: 5,
        flex:1,
        top:0,bottom:0,left:0,right:0
    },
   
    image: {
       height:'100%',
       width:'100%'
    },
});

const DisplayAnImage = (props,route) => {
    console.log('route', props.route.params)

    return (
        <View style={styles.container}>
            
            <Image
                style={styles.image}
                source={{uri: props.route.params}}
                resizeMode='contain'
            />
        </View>
    );
}

export default DisplayAnImage;